from sheetProvider import *
from datetime import datetime

setting_datetimeformat = '%d/%m/%Y %H:%M:%S'
setting_check_in = {'text': 'in', 'color': htmlColorToJSON("#CCFFCC")}
setting_check_out = {'text': 'out', 'color': htmlColorToJSON("#CCCCCC")}

class Log:
    def __init__(self, row, name, card_id, time, check_in):
        self.row = row
        self.name = name
        self.card_id = card_id
        self.time = time
        self.check_in = check_in

    def get_values(self):
        time = '' if self.time == None else str(self.time.strftime(setting_datetimeformat))
        check = setting_check_in['text'] if self.check_in else setting_check_out['text']
        return [[self.name, self.card_id, time, check]]

    def get_colors(self):
        return [[{"backgroundColor": setting_check_in['color'] if self.check_in else setting_check_out['color'] }] * 5]


class LogSheet(SheetProvider):    
    def __init__(self, sheetId=None):
        SheetProvider.__init__(self, GOOGLE_CREDENTIALS_FILE)
        self._logs =[]  
        if sheetId == None:
            self.createSheet()
        else:
            self.setSpreadsheetById(sheetId)
            self.pull()

    def createSheet(self):
        self.create("testpi-Logs", "Logs")
        self.shareWithEmailForWriting("StasiukDanylo@gmail.com")
        print(self.getSheetURL())

    def pull(self):
        self._logs = self.__get_logs()

    def _getLastRow(self):
        return len(self._logs)

    def getLast(self, student=None):
        for i in reversed(range(len(self._logs))):
            if self._logs[i].card_id == student.card_id:
                return self._logs[i]
        return None

    def sendRemote(self, log):
        self.prepare_setValues("A{}".format(str(log.row)), log.get_values())
        self.prepare_setCellsFormats("A{row}:E{row}".format(row=str(log.row)), log.get_colors())             
        self.runPrepared()
    
    def checkIn(self, student):
        if student == None:
            return
        log = None
        lastLog = self.getLast(student)
        if lastLog == None:
            log = Log(self._getLastRow() + 1, student.name, student.card_id, datetime.now(), True)
            self._logs.append(log)
            self.sendRemote(log)
        else:
            log = Log(self._getLastRow() + 1, student.name, student.card_id, datetime.now(), not lastLog.check_in)
            self._logs.append(log)
            self.sendRemote(log)

    def __get_logs(self):
        value = self.getValue("A1:D1000")
        result = []
        for row, i in enumerate(value):
            name = self._safe_list_get(i, 0, '')
            card_id = self._safe_list_get(i, 1, '')
            time = self._safe_list_get(i, 2, '')
            check_in = self._safe_list_get(i, 3, '')
            if name == '' and card_id == '' and time == '' and check_in == '':
                continue
            
            result.append(Log(row + 1, name, card_id, self._safe_date_get(time, setting_datetimeformat, None), check_in == setting_check_in['text']))
        return result

# class end
