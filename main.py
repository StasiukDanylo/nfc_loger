#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Danylo Stasiuk (StasiukDanylo@gmail.com)
# This module uses Google Sheets API v4 (and Google Drive API v3 for sharing spreadsheets)

import os
import RPi.GPIO as GPIO
import MFRC522
import signal
import httplib2
import pygame
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials

from studentSheet import *
from logSheet import *

DEBUG = True


def sound_init():
    pass
    #pygame.mixer.init(44100, -16, 2, 1024)  # setup mixer to avoid sound lag
    #pygame.mixer.music.load('sound.wav')


## MAIN CODE
students = StudentSheet('1d7wtznDK0mRI9U63B6I-DLTpe2qR-_qyBt4CuRAjra0')
logs = LogSheet('1xNGBIlRNQnopsvBZJcQqn_lzvZMH_PNTRmSoRDTZZXM')

# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()

continue_reading = True


# Capture SIGINT for cleanup when the script is aborted
def end_read(signal, frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()


# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)

if __name__ == "__main__":
    sound_init()

    # Welcome message
    print "Welcome to the MFRC522 data read example"
    print "Press Ctrl-C to stop."

    # This loop keeps checking for chips. If one is near it will get the UID and authenticate
    while continue_reading:

        # Scan for cards
        (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

        # Get the UID of the card
        (status, uid) = MIFAREReader.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == MIFAREReader.MI_OK:
            card_id = '{}-{}-{}-{}'.format(str(uid[0]), str(uid[1]), str(uid[2]), str(uid[3]))
            #pygame.mixer.music.play()
            os.system('omxplayer sound.mp3 &')
            if DEBUG:
                print('[{}] Card ID: {}'.format(datetime.now().time(), card_id))
            student = students.getByCardId(card_id)
            if student == None:
                students.add_student(card_id)
            else:
                logs.checkIn(student)
