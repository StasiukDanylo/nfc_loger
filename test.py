#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Danylo Stasiuk (StasiukDanylo@gmail.com)
# This module uses Google Sheets API v4 (and Google Drive API v3 for sharing spreadsheets)

from studentSheet import *
from logSheet import *

students = StudentSheet('13tljpjbyCAkFy3ARS872NJorkrIFB2WNmbf7KgmrRVc')
logs = LogSheet('1kwwxxENaff9TfqJ38SJR7qZo7DtK-BOx9iS9o9Lz54M')

student = students.getByCardId('105-87-79-229')
if student != None:
    logs.checkIn(student)

#ss.check_in('1-2-3-3')