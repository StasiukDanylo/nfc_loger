from sheetProvider import *

class Student:
    def __init__(self, row, name, card_id):
        self.row = row
        self.name = name
        self.card_id = card_id

    def get_values(self):
        return [[self.name, self.card_id]]
  

class StudentSheet(SheetProvider):    
    def __init__(self, sheetId=None):
        SheetProvider.__init__(self, GOOGLE_CREDENTIALS_FILE)
        self._students =[]
        if sheetId == None:
            self.createSheet()
        else:
            self.setSpreadsheetById(sheetId)
            self.pull()

    def createSheet(self):
        self.create("testpi-Students", "Students")
        self.shareWithEmailForWriting("StasiukDanylo@gmail.com")
        print(self.getSheetURL())

    def getLast(self, student=None):
        for i in reversed(range(len(self._students))):
            if self._students[i].card_id == student.card_id:
                return self._students[i]
        return None

    def sendRemote(self, student):
        self.prepare_setValues("A{}".format(str(student.row)), student.get_values())           
        self.runPrepared()

    def pull(self):
        self._students = self.__get_students()

    def _getLastRow(self):
        return len(self._students)


    def __get_students(self):
        value = self.getValue("A1:B1000")
        result = []
        for row, i in enumerate(value):
            name = self._safe_list_get(i, 0, '')
            card_id = self._safe_list_get(i, 1, '')
            if name == '' and card_id == '':
                continue
            result.append(Student(row + 1, name, card_id))
        return result

    def add_student(self, card_id):
        student = Student(self._getLastRow() + 1, '', card_id)
        self._students.append(student)
        self.sendRemote(student)

    
    def getByCardId(self, card_id):
        for student in self._students:
            if student.card_id == card_id:
                return student
        return None
